from django import forms

from fileupload.models import File


class FileForm(forms.ModelForm):
    class Meta:
        model = File
        fields = ['file', 'file_type', 'description']