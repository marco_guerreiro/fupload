# encoding: utf-8
import time

import os
from django.conf import settings
from django.core.validators import FileExtensionValidator
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.urls import reverse


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'files/users/user_{0}/{1}/{2}'.format(instance.id, str(int(time.time())), filename)


class File(models.Model):
    file_types = (
        ('sale', 'Agreement of Purchase & Sale'),
        ('adjustments', 'Statement of Adjustments'),
        ('lease', 'Lease Agreement'),
        ('other', 'Other')
    )
    file = models.FileField(
        upload_to=user_directory_path,
        validators=[FileExtensionValidator(
            ["pdf", "doc", "docx", "jpeg", "jpg", "bmp", "png", "tx t", "tiff"],
            "Files need to be in PDF, Word or Image format")
        ])
    file_type = models.CharField(max_length=30, choices=file_types)
    description = models.CharField(max_length=50, null=True, blank=True)
    # uploaded_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="uploaded_by")
    upload_date = models.DateTimeField(auto_now_add=True)
    update_upload_date = models.DateTimeField(auto_now=True)
    # property = models.ForeignKey(Properties)
    # building = models.ForeignKey(Buildings)
    # user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="user_owner")
    # lease = models.ForeignKey(Lease)
    active = models.BooleanField(default=True)
    private = models.BooleanField(default=True)

    def view_file_url(self):
        return reverse('file_view', args=(self.id, ))


@receiver(post_delete, sender=File)
def delete_files(sender, instance, **kwargs):
    if os.path.exists(instance.file.path):
        os.remove(instance.file.path)