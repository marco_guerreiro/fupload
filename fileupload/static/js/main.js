$(function () {
    'use strict';

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });
    var fileUpload = $('#fileupload');

    // Initialize the jQuery File Upload widget:
    fileUpload.fileupload({
        acceptFileTypes: /([.\/])(pdf|doc?x|jpe?g|tiff|gif|bmp|png)$/i,
        maxFileSize: 15000000,
        destroy: function (e, data) {
            if (confirm('Do you want to delete this file?')) {
                var that = $(this).data('blueimp-fileupload') ||
                        $(this).data('fileupload'),
                    removeNode = function () {
                        that._transition(data.context).done(
                            function () {
                                $(this).remove();
                                that._trigger('destroyed', e, data);
                            }
                        );
                    };
                if (data.url) {
                    $.ajax(data).done(removeNode);
                } else {
                    removeNode();
                }
            }
        }
    })
        .bind('fileuploadsubmit', function (e, data) {
            var row = data.context;
            var type = row.find('.file-type').val();
            var description = row.find('.description').val();
            if (type === 'other' && !description) {
                return false;
            } else if (!type) {
                alert('Choose Document Type');
                return false
            }
            data.formData = {description: description, file_type: type}
        })
        .bind('fileuploadadded', function (e, data) {
            data.context.find('.file-type').html(fileTypes)
        });

    fileUpload.addClass('fileupload-processing');
    $.ajax({
        url: '/upload/view/',
        dataType: 'json',
        context: fileUpload[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, null, {result: result});
    });
    function handleFileType(obj) {
        var tr = obj.closest('tr');
        var description = tr.find('.description');
        var type = tr.find('.file-type').val();
        var button = tr.find('button.start');
        description.attr('placeholder', type === 'other'? 'Description' : 'Description (optional)');
        if (!type || type === 'other' && !description.val()) {
            button.css('visibility', 'hidden');
            button.prop('disabled', true);
            return false;
        }
        button.css('visibility', 'visible');
        button.prop('disabled', false);
        return true;
    }
    fileUpload
        .on('keyup', '.description', function (e) {
            handleFileType($(this))
        })
        .on('change', '.file-type', function (e) {
            handleFileType($(this))
        })
});
