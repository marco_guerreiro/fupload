# encoding: utf-8
from django.conf.urls import url
from fileupload.views import file_create_view, FileDeleteView, FileListView, file_view

urlpatterns = [
    url(r'^new/$', file_create_view, name='upload-new'),
    url(r'^delete/(?P<pk>\d+)$', FileDeleteView.as_view(), name='upload-delete'),
    url(r'^view/$', FileListView.as_view(), name='upload-view'),
    url(r'^view_file/(?P<pk>[0-9]+)/$', file_view, name='file_view')
]
