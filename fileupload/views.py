# encoding: utf-8
import json

from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.views.generic import CreateView, DeleteView, ListView

from fileupload.forms import FileForm
from .models import File
from .response import JSONResponse, response_mimetype
from .serialize import serialize


def file_create_view(request):
    form = FileForm(request.POST or None, files=request.FILES or None)
    if form.is_valid():
        obj = form.save()
        files = [serialize(obj)]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

    if request.method == 'POST':
        data = json.dumps(form.errors)
        return HttpResponse(content=data, status=400, content_type='application/json')

    return render(request, 'fileupload/file_form.html', {'file_types': File.file_types})


class FileDeleteView(DeleteView):
    model = File

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.delete()
        response = JSONResponse(True, mimetype=response_mimetype(request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class FileListView(ListView):
    model = File

    def render_to_response(self, context, **response_kwargs):
        files = [serialize(p) for p in self.get_queryset()]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


def file_view(request, pk):
    file = get_object_or_404(File, pk=pk)
    return HttpResponse(file.file.path)
